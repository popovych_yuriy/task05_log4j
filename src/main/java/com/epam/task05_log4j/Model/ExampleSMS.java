package com.epam.task05_log4j.Model;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "AC8f34d7909054baea29a9f9de8fd380e9";
    public static final String AUTH_TOKEN = "bc89716dd10225d812f4de91fe83e3a2";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+38095*******"), /*my phone number*/
                        new PhoneNumber("+13215748759"), str).create(); /*attached to me number*/
    }
}
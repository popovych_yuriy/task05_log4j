package com.epam.task05_log4j;

import com.epam.task05_log4j.Model.*;

public class Application {
    public static void main(String[] args) {
        Model model;

        // Task 1 - The log information should be displayed in the console and saved to file.
        // Task 3.1 - File will not be overwritten.
        model = new Task1();
        model.makeLog();

        // Task 2 - Make different appenders for debug and info
        model = new Task2();
        model.makeLog();

        // Task 3.2 - File will be overwritten every day
        model = new Task32();
        model.makeLog();

        // Task 3.3 - File will be overwritten after reaching the size of 1MB
//        model = new Task33();
//        for (int i = 0; i <4000 ; i++) {
//            model.makeLog();
//        }

        // Task 4 - Configure logger to that all levels higher than "WARN" will be saved in the file.
        model = new Task4();
        model.makeLog();

        // Task 5 - Configure logger to that in the file will be recorded only "WARN", and in the console - only "INFO"
        model = new Task5();
        model.makeLog();

        // Task 6 - Configure logger that "ERROR" will be sent on e-mail
        model = new Task6();
        model.makeLog();

        // Task 7 - Configure logger that "FATAL" will be sent via SMS
        model = new Task7();
        model.makeLog();
    }
}
